const TRUE_FALSE = 'trueFalse';
const MCQ_QUESTION = 'mcqQuestion';
const MATCHING = 'matching';
const FILL_IN_BLANK = 'fillInBlank';
const RATING = 'rating';
const CHAPTERS = 'chapters';
const _ID = '_id';
const QUESTIONS = 'questions';
const QUESTION = 'question';
const PICK_COUNT = 'pickCount';
const DATA = 'data';
const SCORE = 'score';
const ATTEMPTS = 'attempts';
const SUCCESS = 'success';
const PROGRESS = 'progress';
const COMPLETE = 'complete';
const LOCKED = 'locked';
const USER = 'user';
const PUBLISHER = 'publisher';
const MODULE_ID = 'moduleId';
module.exports = {
    TRUE_FALSE,
    MCQ_QUESTION,
    MATCHING,
    FILL_IN_BLANK,
    RATING,
    CHAPTERS,
    _ID,
    QUESTIONS,
    PICK_COUNT,
    QUESTION,
    DATA,
    SCORE,
    ATTEMPTS,
    SUCCESS,
    PROGRESS,
    COMPLETE,
    LOCKED,
    USER,
    PUBLISHER,
    MODULE_ID
};             