const metadata = {
    fillInBlankPlaceHolder: '',
    modulesArray: [],
    minimumPassPercent:0,
    ratingQuestions:[],
    mcqQuestions:[],
    trueFalseQuestions:[],
    fillInBlankQuestions:[]

};

module.exports = metadata;